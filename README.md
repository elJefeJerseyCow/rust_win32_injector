# Win32 Injector
Win32 Injector is a tool that allows shellcode binary files — pure shellcode data — to be injected into Win32 processes. It's a demonstration tool for the purpose of learning how to interface Rust, Windows-rs and the Win32 API.

It is not meant to be sophisticated — only a demonstration.

## Usage
The tool operates in two modes, the first uses it's current process ID and injects shellcode into itself.

`rust_win32_injector.exe -c -s .\shellcode_pop_calc.bin`

The second mode uses a specificed process ID.

`rust_win32_injector.exe -p 1234 .\shellcode_pop_calc.bin`

The tool is not meant to exist cleanly, only remonstrate the concept and interactions with Win32 APIs and Rust.

## Building
Install the Rust environment and then type `cargo build --release` to compile and link the Rust course code.