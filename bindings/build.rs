fn main() {
    windows::build!(
        // Common
        Windows::Win32::System::SystemServices::{BOOL, HANDLE, PSTR, HINSTANCE},
        // Thread
        Windows::Win32::System::SystemServices::LPTHREAD_START_ROUTINE,
        Windows::Win32::System::Threading::CreateRemoteThread,
        // Security
        Windows::Win32::System::SystemServices::SECURITY_ATTRIBUTES,
        // Process
        Windows::Win32::System::Threading::{GetCurrentProcessId, OpenProcess, PROCESS_ALL_ACCESS},
        Windows::Win32::System::SystemServices::{GetModuleHandleA, GetProcAddress, FARPROC},
        // Memory
        Windows::Win32::System::Diagnostics::Debug::WriteProcessMemory,
        Windows::Win32::System::Memory::{VirtualAllocEx, MEM_RESERVE, MEM_COMMIT},
        Windows::Win32::System::SystemServices::PAGE_EXECUTE_READWRITE,
        // I/O
        Windows::Win32::Storage::FileSystem::GetFullPathNameA,
    );
}