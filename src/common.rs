use crate::safe_api::process;
use clap::{App, Arg};
use std::fs;
use std::path::Path;
use std::{path::PathBuf, u32};

#[derive(Debug)]
pub struct Config {
    pub process_id: u32,
    pub shellcode: PathBuf,
}

impl Config {
    pub fn new() -> Result<Self, &'static str> {
        let matches = App::new("Uggs C2")
            .version("0.1.0")
            .arg(
                Arg::with_name("process_id")
                    .short("p")
                    .long("process_id")
                    .help("Process ID to injection into")
                    .takes_value(true),
            )
            .arg(
                Arg::with_name("shellcode")
                    .short("s")
                    .long("shellcode")
                    .help("shellcode to execute in process")
                    .takes_value(true),
            )
            .arg(
                Arg::with_name("current_pid")
                    .short("c")
                    .conflicts_with("process_id")
                    .help("Inject into the current process"),
            )
            .get_matches();

        // Process ID doesn't need to be mutable as it's unitialised
        let process_id: u32;

        // Extract current process ID
        if matches.is_present("current_pid") {
            process_id = process::get_current_process_id();
        // Extract provided process ID
        } else {
            if let Some(process_id_str) = matches.value_of("process_id") {
                if let Ok(process_id_u32) = process_id_str.parse::<u32>() {
                    process_id = process_id_u32
                } else {
                    return Err("Cannot parse process_id '{}', process_id");
                }
            } else {
                return Err("process_id has not been provided");
            }
        }

        // Shellcode doesn't need to be mutable as it's uninitialised
        let shellcode: &Path;

        // Extract shellcode binary file path
        if let Some(shellcode_str) = matches.value_of("shellcode") {
            shellcode = Path::new(shellcode_str)
        } else {
            return Err("shellcode has not been provided");
        }

        // Check exists
        if !shellcode.is_file() {
            panic!(format!(
                "Shellcode file {:?} does not exist",
                shellcode.as_os_str()
            ));
        }

        // Ensure conversion to full path
        let shellcode = if let Ok(path) = fs::canonicalize(shellcode) {
            path
        } else {
            panic!(format!(
                "Cannot canonicalize path {:?}",
                shellcode.as_os_str()
            ));
        };

        Ok(Self {
            process_id,
            shellcode,
        })
    }
}
