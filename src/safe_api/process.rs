use std::io::Error;

use bindings::Windows::Win32::System::{
    SystemServices::HANDLE,
    Threading::{GetCurrentProcessId, OpenProcess, PROCESS_ALL_ACCESS},
};

pub fn get_current_process_id() -> u32 {
    unsafe { GetCurrentProcessId() }
}

pub fn open_process(process_id: u32) -> Result<HANDLE, String> {
    // Attempt to open process
    let handle = unsafe { OpenProcess(PROCESS_ALL_ACCESS, false, process_id) };

    if handle.is_null() {
        Err(format!(
            "Cannot open process — {}",
            Error::last_os_error().to_string()
        ))
    } else {
        Ok(handle)
    }
}
